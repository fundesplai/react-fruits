import { useState } from 'react';

const productes = [
    {
        "id": 1,
        "nom": "Plàtan",
        "preu": 0.5
    },
    {
        "id": 2,
        "nom": "Poma",
        "preu": 0.8
    },
    {
        "id": 3,
        "nom": "Pinya",
        "preu": 5
    },
    {
        "id": 4,
        "nom": "Meló",
        "preu": 6
    },
];



function Cosa() {

    const [tiquet, setTiquet] = useState([]);


    function compra(f) {
        //console.log("estan comprando fruta" + f.nom);
        setTiquet([...tiquet, f]);
    }



    function muestraFruta(x) {
        return (
            <div className="fruta">
                <h1>{x.nom}</h1>
                <h3>{x.preu}</h3>
                <button onClick={() => compra(x)}>Comprar</button>
            </div>
        );
    }

    let frutas = productes.map(muestraFruta)


    function muestraTiquet(x) {
        return (
            <div className="fruta">
                <h1>{x.nom}</h1>
            </div>
        );
    }

    let compras = tiquet.map(muestraTiquet)



    return (
        <>

            <h1>Cosa</h1>


            {frutas}
            <br />
            <br />
            <br />
            {compras}


        </>
    )
}



export default Cosa;